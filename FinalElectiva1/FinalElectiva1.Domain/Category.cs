﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FinalElectiva1.Domain
{
    public class Category
    {
        [Key]
        public int CatergoryId { get; set; }

        [Required(ErrorMessage ="El Campo {0} esta Vacio")]
        [Index("Category_Description_Index", IsUnique = true)]

        public string Descripcion { get; set; }

    }
}
